import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

/*
  Generated class for the StorageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
const STORAGE_KEY = 'TASKS';

@Injectable()
export class StorageProvider {

  constructor(public storage: Storage) {}

  public addTask(item:string){
    return this.getTasks().then((tasks) => {
      if(tasks){
        tasks.push(item);
        return this.storage.set(STORAGE_KEY, tasks);
      } else {
        return this.storage.set(STORAGE_KEY, [item]);
      }
    });
  }

  public removeTask(index:number){
    return this.getTasks().then((tasks) => {
      if(tasks){
        tasks.splice(index, 1);
        return this.storage.set(STORAGE_KEY, tasks);
      }
    });
  }

  public getTasks(){
    return this.storage.get(STORAGE_KEY);
  }

}
