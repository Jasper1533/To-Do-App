import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';

/**
 * Generated class for the TasksPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tasks',
  templateUrl: 'tasks.html',
})
export class TasksPage {
  inputTask: string;
  tasks;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storageProvider: StorageProvider) {
    this.getTasks();
  }

  addTask(){
    this.storageProvider.addTask(this.inputTask).then(() => this.getTasks());
  }

  removeTask(index:number){
    this.storageProvider.removeTask(index).then(() => this.getTasks());
  }

  getTasks(){
    this.storageProvider.getTasks().then((tasks) => this.tasks = tasks);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TasksPage');
  }

}
